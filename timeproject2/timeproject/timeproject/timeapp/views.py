from django.http import HttpResponse

from time import time as t

import json

def time(request):
    return HttpResponse(round(t()))

def quest(request):
    result = {}
    result['id'] = 1
    result['type'] = 'build'
    result['color'] = 'white'
    result['count'] = 1
    result['rewardColor'] = 'yellow'
    result['rewardAmount'] = 100
    return HttpResponse(json.dumps(result))
	
def quest2(request):
    result = {}
    result['id'] = 2
    result['type'] = 'build'
    result['color'] = 'yellow'
    result['count'] = 1
    result['rewardColor'] = 'red'
    result['rewardAmount'] = 100
    return HttpResponse(json.dumps(result))


def quest3(request):
    result = {}
    result['id'] = 3
    result['type'] = 'gather'
    result['color'] = 'red'
    result['count'] = 100
    result['rewardColor'] = 'purple'
    result['rewardAmount'] = 100
    return HttpResponse(json.dumps(result))
	

def quest4(request):
    result = {}
    result['id'] = 4
    result['type'] = 'build'
    result['color'] = 'white'
    result['count'] = 2
    result['rewardColor'] = 'yellow'
    result['rewardAmount'] = 200
    return HttpResponse(json.dumps(result))
	

def quest5(request):
    result = {}
    result['id'] = 5
    result['type'] = 'gather'
    result['color'] = 'white'
    result['count'] = 200
    result['rewardColor'] = 'red'
    result['rewardAmount'] = 300
    return HttpResponse(json.dumps(result))