from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^quest/$', views.quest, name='quest'),
    url(r'^$', views.time, name='time'),
	
	url(r'^quest2/$', views.quest2, name='quest2'),
    url(r'^$', views.time, name='time'),
	
	url(r'^quest3/$', views.quest3, name='quest3'),
    url(r'^$', views.time, name='time'),
	
	url(r'^quest4/$', views.quest4, name='quest4'),
    url(r'^$', views.time, name='time'),
	
	url(r'^quest5/$', views.quest5, name='quest5'),
    url(r'^$', views.time, name='time'),
]